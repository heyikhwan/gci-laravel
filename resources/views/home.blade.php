<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Garuda Cyber Institute</title>
</head>
<body>
    <h1>Garuda Cyber Institute</h1>
    <h3>Jadilah Programmer Handal Bersama GC-INS</h3>
    <p>Grow Together With Garuda Cyber Institute</p>

    <b>Syarat dan Ketentuan</b>
    <ul>
        <li>Tamatan SMA/SMK</li>
        <li>Tamatan Perguruan Tinggi</li>
        <li>Pekerja IT</li>
        <li>Freelancer</li>
    </ul>

    <b>Cara Bergabung</b>
    <ol>
        <li>Kunjungi website GC-INS</li>
        <li>Register</li>
        <li>Lakukan Pembayaran</li>
    </ol>

    <span>Segera daftarkan diri Anda! </span> <a href="{{ route('register') }}">klik untuk register!</a>
</body>
</html>