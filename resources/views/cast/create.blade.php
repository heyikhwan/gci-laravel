@extends('layouts.app')

@section('title', 'Cast')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('cast.store') }}" method="post">
                    @csrf

                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" placeholder="Masukkan Nama" required>
                        @error('nama')
                            {{ $message }}
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="number" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur" placeholder="Masukkan umur" required>
                        @error('umur')
                            {{ $message }}
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <textarea name="bio" id="bio" rows="3" class="form-control" placeholder="Masukkan Bio" style="resize: none" required></textarea>
                        @error('bio')
                            {{ $message }}
                        @enderror
                    </div>

                    <div class="d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection