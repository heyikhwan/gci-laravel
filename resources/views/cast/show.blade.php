@extends('layouts.app')

@section('title', $cast->nama)

@section('content')
<div class="row">
    <div class="col-6">
        <div class="card">
            <div class="card-body">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>Nama</th>
                            <td>:</td>
                            <td>{{ $cast->nama }}</td>
                        </tr>
                        <tr>
                            <th>Umur</th>
                            <td>:</td>
                            <td>{{ $cast->umur }} tahun</td>
                        </tr>
                        <tr>
                            <th>Bio</th>
                            <td>:</td>
                            <td>{{ $cast->bio }}</td>
                        </tr>
                    </tbody>
                </table>

                <a href="{{ route('cast.index') }}" class="btn btn-secondary mt-3">Kembali</a>
            </div>
        </div>
    </div>
</div>
@endsection