<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form Pendaftaran</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="{{ route('welcome') }}" method="get">
        @csrf

        <div>
            <label for="first_name">First name:</label><br />
            <input type="text" name="first_name" id="first_name" required />
        </div>

        <br />

        <div>
            <label for="last_name">Last name:</label><br />
            <input type="text" name="last_name" id="last_name" required />
        </div>

        <br />

        <div>
            <label>Gender:</label>
            <div>
                <input type="radio" name="gender" id="male" value="Male" />
                <label for="male">Male</label>
            </div>

            <div>
                <input type="radio" name="gender" id="female" value="Female" />
                <label for="female">Female</label>
            </div>

            <div>
                <input type="radio" name="gender" id="other" value="Other" />
                <label for="other">Other</label>
            </div>
        </div>

        <br />

        <div>
            <label for="nationality">Nationality</label>
            <div>
                <select name="nationality" id="nationality">
                    <option value="Indonesia">Indonesia</option>
                    <option value="Malaysia">Malaysia</option>
                    <option value="Singapore">Singapore</option>
                    <option value="Thailand">Thailand</option>
                </select>
            </div>
        </div>

        <br />

        <div>
            <label>Lenguage Spoken:</label><br />

            <input type="checkbox" name="lenguage" id="bahasa_indonesia" value="Bahasa Indonesia" />
            <label for="bahasa_indonesia">Bahasa Indonesia</label>
            <br />
            <input type="checkbox" name="lenguage" id="english" value="English" />
            <label for="english">English</label>
            <br />
            <input type="checkbox" name="lenguage" id="other_1" value="Other" />
            <label for="other_1">Other</label>
        </div>

        <br />

        <div>
            <label for="bio">Bio:</label><br />
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        </div>

        <br />

        <button type="submit">Sign Up</button>
    </form>
</body>

</html>