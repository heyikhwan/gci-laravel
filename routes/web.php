<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('register', [AuthController::class, 'register'])->name('register');

Route::get('welcome', [AuthController::class, 'welcome'])->name('welcome');

Route::get('table', function() {
    return view('table');
})->name('table');

Route::get('datatable', function() {
    return view('datatable');
})->name('datatable');

Route::get('cast', [CastController::class , 'index'])->name('cast.index');
Route::get('cast/create', [CastController::class , 'create'])->name('cast.create');
Route::post('cast', [CastController::class , 'store'])->name('cast.store');
Route::get('cast/{cast}', [CastController::class , 'show'])->name('cast.show');
Route::get('cast/{cast}/edit', [CastController::class , 'edit'])->name('cast.edit');
Route::put('cast/{cast}', [CastController::class , 'update'])->name('cast.update');
Route::delete('cast/{cast}', [CastController::class , 'destroy'])->name('cast.destroy');

